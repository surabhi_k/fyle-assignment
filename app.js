var app = angular.module("fyle_app", []);

app.controller('fyle_controller', function($scope, $http, $filter) {
    
    // variables
    $scope.cities = ['Bangalore', 'Mumbai', 'Delhi', 'Chennai', 'Hyderabad', 'Pune', 'Raipur', 'Bhopal', 'Nagpur', 'Mysore'];
    $scope.selectedCity = "Bangalore";
    $scope.api_results = {};
    $scope.filtered_api_results = [];
    $scope.search_key = "";
    
    // methods
    $scope.clear_change = function(){
        $scope.filtered_api_results = [];
    };

    $scope.change = function(search_key) {
        $scope.clear_change();
        var len = $scope.api_results.length;
        for (var i = 0; i < len; i++) {
            if (($scope.api_results[i].bank_name.search($filter('uppercase')($scope.search_key)) != -1) || ($scope.api_results[i].branch.search($filter('uppercase')($scope.search_key)) != -1) || ($scope.api_results[i].address.search($filter('uppercase')($scope.search_key)) != -1) || ($scope.api_results[i].ifsc.search($filter('uppercase')($scope.search_key)) != -1)) {
                $scope.filtered_api_results.push($scope.api_results[i]);
            }  
        }
    };

    $scope.selectCity = function(city_inp) {
        $scope.selectedCity = city_inp;
        $scope.bankApi(city_inp);
    };

    $scope.bankApi = function(selectedCity) {
        var city = $filter('uppercase')(selectedCity);
        $http.get("https://api.fyle.in/api/bank_branches?city=" + city + "&offset=0&limit=50")
            .then(function(response) {
                $scope.clear_change();
                $scope.api_results = response.data;
                $scope.filtered_api_results = response.data;
                $scope.change($scope.search_key);
            });
        };

});